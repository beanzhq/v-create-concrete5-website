# v-create-concrete5-website

Creates a blank Concrete5 website on the given domain of the user.

Options:

- USER (The VestaCP user the website is for)
- DOMAIN (The domain of the website. Already created under the given user)
- DATABASE (The database name. Already created under the given user)
- DBUSER (The database user)
- DBPASS (The database password)

Extra optional options:

- SITE (The site name)
- EMAIL (The admin e-mail address)
- C5RELEASE (The code of the c5 release that should be used)

Example:
```
v-create-concrete5-website wardhache wardhache.be wardhache_conc1 wardhache_conc1 password;
```
